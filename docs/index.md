## A propos de moi 
Bonjour et bienvenue sur ma page web FabZero !

Je m'appelle Jonathan Kahan et j'ai 22 ans. 

![](images/cv_small2.jpg)

## Formation 
Je suis actuellement étudiant en deuxième année de master pour devenir **ingénieur électromécanicien** à l'ULB. Ma spécialisation est *Sustainable Transport and Automotive Engineering*. 

Je réalise actuellement mon mémoire sur les boîtes de vitesses pour véhicules électriques en collaboration avec une entreprise *leader* dans ce domaine. 

## Pourquoi avoir choisi ce cours *FabZero* ?
J'ai toujours eu un attrait particulier pour la mécanique et pour le fait de réparer toutes sortes d'objets (tondeuse, vitre de voiture, sèche-linge,...) ! Pour réparer tous ces objets, je m'aide souvent d'une imprimante 3D qui me permet d'imprimer des pièces plutôt que de devoir les acheter (souvent en ligne). 

Par conséquent, je me suis inscrit au cours *FabZero* principalement pour:

* Apprendre d'autres outils que l'impression 3D en fabrication numérique 
* Apprendre des alternatives open-source aux logiciels que je connais actuellement (en particulier SolidWorks, car je perdrai ma licence étudiante l'année prochaine)
* Apprendre des techniques pour organiser et gérer mon travail (*project management*)

## Projets réalisés
* Réalisation d'une prothèse de main intelligente 
* Programmation d'un jeu vidéo (*tower defense*)
* Régulation PI d'un *air heater*
* Calcul des contraintes dans un support moteur par méthode des éléments finis
* Divers projets de réparation grâce à l'impression 3D
* ...


## Hobbies

* Formule 1 (particulièrement le développement technique)
* Padel 
* Cuisine 
* Réparation et projets *DIY*
*  ...