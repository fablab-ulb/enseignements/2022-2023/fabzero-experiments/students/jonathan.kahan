# 5.Team dynamics and final projet

## 5.1 Project Analysis and Design
D'après [cette source](https://wikis.ec.europa.eu/display/ExactExternalWiki/Problem+and+objective+tree#:~:text=What%20is%20it%3F,corresponding%20objectives%20(positive%20situation).): The problem tree is a graphic tool helping to structure hierarchically problems identified (the negative situation), clarifying their cause effect relationship. The objective tree is the positive interface of the problem tree, hierarchically organizing the corresponding objectives (positive situation). 

J'ai décidé de créer un arbre de problème sur le sujet des déchets plastiques à usage unique, sur base de [ce projet](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class-website/-/blob/main/docs/final-projects/img/anais.derue-benjamin.hainaut-presentation.pdf). 

<div style="text-align:center">
  <img src="../images/module_5/ArbreProblemModule5.png" alt="" style="width:70%;">
</a>  
</div>

Le tronc central est le problème: **L'impact environnemental des plastiques à usages unique**

En dessous se trouvent les racines/les causes du problèmes. 

Au dessus se trouvent les conséquences de celui-ci. 

La lecture de l'arbre est explicite et ne demande pas de commentaire en particulier. 

Sur base de l'arbre des problèmes l'arbre des objectifs peut-être créé. 

<div style="text-align:center">
  <img src="../images/module_5/ArbreSolutionModule5.png" alt="" style="width:50%;">
</a>  
</div>

Afin de garder de la lisibilité, je n'ai gardé que la première "couche" des résultats (en vert).

## 5.2 Group formation and brainstorming on project problems
Pour cette séance, chaque participant a été invité à apporter un objet qui représente une problématique qui leur tient à cœur. Une fois que tous les objets ont été présentés, l'objectif était de trouver des personnes qui avaient apporté des objets ayant une thématique commune avec le sien.

Lors de l'activité en groupe, j'ai choisi d'apporter ma gourde. Pour moi, les enjeux climatiques actuels sont une problématique qui me tient particulièrement à cœur. En apportant ma gourde, j'ai voulu montrer que je m'engageais à réduire ma consommation de plastique et que je souhaitais sensibiliser les autres participants à cette question importante. 

### 5.2.1 Formation du groupe

Au moment de former les groupes, j'ai remarqué que plusieurs participants avaient également apporté une gourde, montrant ainsi leur préoccupation pour les enjeux climatiques. Nous avons donc décider de former un groupe ensemble. Lucie s'est également jointe à notre groupe en apportant un café froid dans un gobelet jetable en plastique. Le groupe est donc composé de [Thomas](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/thomas.marechal/), [Gilles](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/gilles.theunissen/), [Jules](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jules.gerard/) et [Lucie](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/Lucie.lestienne/). Nous avons tous un background différent, je pense que cela sera un vrai avantage lors du projet. 

### 5.2.2 Choix de la thématique et de la problématique 

Nous avons pris quelques minutes pour écrire des mots sur une feuille de papier qui, selon nous, étaient en lien avec nos objets. Ensuite, nous avons chacun écrit sur une note post-it un thème qui nous tenait à cœur, discuté de ce qui nous motivait dans ce thème, et choisi l'un d'entre eux pour passer à l'étape suivante. Sans surprise, les liens qui ont émergé étaient principalement liés à l'eau, au recyclage, à l'écologie, à la portabilité, etc.

<div style="text-align:center">
  <img src="../images/module_5/image1.png" alt="" style="width:40%;">
</a>  
</div>

Au début, nous avons eu du mal à choisir un thème et allions directement vers des problématiques. **Nous avons finalement opté pour le recyclage comme thème général.** Nous n'avons pas réussi à trouver des problématiques pertinentes dans le temps imparti (nous commencions à dire n'importe quoi en espérant stimuler des idées) mais voici le processus/raisonnement pour passer d'un thème à un ensemble de problématiques.
L'idée était de prendre un exemple concret d'un problème lié au recyclage, comme "l'impact environnemental de la production de bouteilles en plastique". À partir de là, nous devions dériver une problématique de ce thème. Le cheminement est le suivant.

- Pourquoi la production de bouteilles en plastique est-elle un problème pour l'environnement? Parce que cela pollue.
- Que signifie polluer? Émettre des émissions de CO2, polluer physiquement les océans avec du plastique, etc.
- Pourquoi la pollution des océans par le plastique est-elle un problème? Parce que les microplastiques tuent les poissons.
etc.

La récursivité des questions "pourquoi?" et "comment?" a permis de passer du thème "l'impact environnemental de la production de bouteilles en plastique" (qui n'est pas une question) à la problématique "comment réduire la présence de microplastiques dans les océans?" (une question).

Pour la dernière étape, nous avons choisi un porte-parole pour se rendre dans les autres groupes et expliquer la thématique choisie par notre groupe afin de collecter de nouvelles idées de problématiques. J'ai été désigné comme porte-parole et j'ai visité deux autres groupes. Bien qu'ils aient mentionné des problématiques similaires à celles que nous avions identifiées, ainsi que des nouvelles, cela restait assez vague. Nous n'avions pas encore une idée précise de la problématique. Voici cependant les idées qui ont émergées. 

<div style="text-align:center">
  <img src="../images/module_5/image2.png" alt="" style="width:40%;">
</a>  
</div>

## 5.3 Group dynamics

### 5.3.1 Decision methods
- Consensus: Tout le monde est d'accord sur une option.
- Tirage au sort: https://plouf-plouf.fr/.
- Votes: cela peut être pondéré. Par exemple, chaque membre du groupe dispose de 3 votes qu'il doit attribuer à 5 options différentes. L'option ayant le score total le plus élevé l'emporte. Bien sûr, vous pouvez également utiliser un vote normal avec une seule voix.
- Décision à la majorité
- Consentement / accord: je préfère une autre option, mais j'accepte une autre option même si ce n'est pas ma préférée. C'est une décision sans objection!
- Vérification de température: vérifier la température de chaque personne avec la main. Lever la main assez haut signifie que vous êtes d'accord avec cela. En revanche, la garder assez bas signifie que vous n'êtes pas d'accord. Vous pouvez moduler votre désir en changeant la hauteur de la main.

Nous avons convenu en groupe de privilégier l'option du consensus dans un premier temps. Si le consensus n'est pas atteint, nous opterons alors pour un vote pondéré.

### 5.3.2 Give feedback

Nous pouvons donner des retours sur le fonctionnement du groupe dans son ensemble et sur les performances individuelles. Pour communiquer efficacement, nous pouvons utiliser les principes de la communication non violente, qui comprennent le fait de discuter de nos propres expériences, de se concentrer sur des informations factuelles, d'exprimer nos émotions et de suggérer des solutions possibles.

Par exemple, dans le cas d'un membre qui arrive régulièrement en retard, nous pouvons dire : "Je me sens frustré car le retard du membre cause des problèmes logistiques pour moi. Je suggère que nous retardions chaque réunion de 30 minutes ou que nous encourageions tout le monde à être ponctuel."

### 5.3.3 Meteo check 

le principe de "météo check" ou "check-in météo" consiste à faire un tour de table au début d'une réunion pour permettre à chaque membre de partager son humeur, son état d'esprit ou tout autre sentiment ou émotion qui pourrait influencer sa participation à la réunion. Cette pratique permet d'encourager la transparence, la communication ouverte et l'empathie entre les membres d'un groupe, et peut contribuer à améliorer la qualité de la participation et des échanges au sein de la réunion.