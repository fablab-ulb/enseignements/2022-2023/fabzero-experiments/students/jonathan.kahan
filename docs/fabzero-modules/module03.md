# 3. Impression 3D

- Identifier les avantages et les limites de l'impression 3D.
- Appliquer des méthodes de conception et des processus de production utilisant l'impression 3D.

## 3.1 La technologie

Nous utiliserons la technique la plus courante d'impression 3D, l'impression 3D par extrusion de matériau.

Comme on peut le lire [ici](https://fr.wikipedia.org/wiki/D%C3%A9p%C3%B4t_de_fil_fondu) : "Le dépôt de fil fondu (DFF) ou Fused deposition modeling (FDM) ou encore Fused Filament Fabrication (FFF)3 est une technologie d'impression 3D. On appelle aussi ce procédé impression 3D par extrusion de matériau.

Cette technologie consiste à déposer de la matière à l'état liquide couche par couche. La technologie utilise le plus souvent un filament de matière polymère qui est fondu puis extrudé pour construire une pièce couche par couche. Le matériau d’apport est extrudé au travers d’une buse sur la table de la machine. Le mouvement de la table ou de la buse permet de créer une couche de matériau ayant la géométrie désirée. La succession des différentes couches permet de créer la pièce finale."

## 3.2 Les matériaux

Il existe différents matériaux pour l'impression 3D. En fonction du matériaux, il faut adapter les paramètres de l'impression, en particulier, la température. Toutes ces informations sont disponibles [ici](https://blog.prusa3d.com/advanced-filament-guide_39718/).


## 3.3 Torture test (kind of)

Comme expliqué dans le module 2 de mon blog, le but final est d'imprimer une voiture avec suspension en collaboration avec Cédric et Dimitri. Je m'occupe d'imprimer le chassi de la voiture. La pièce flexible de ma partie est l'antenne de la voiture. Afin d'être certain que l'antenne sera bien flexible et que sont impression sera réusssi sur la pièce finale, j'ai réalisé un test pour cette partie spécifiquement avant d'imprimer la pièce en entier (ce qui requiert du temps, de la matière et de l'énergie). 

J'ai donc créer une pièce "test" pour l'antenne de la voiture comme ceci: 
```
$fn = 250;

cube([15,70,10]);

for (i = [1:0.5:5]) {
     translate([5,12*i,8])
        rotate([30,0,90])
            cylinder(h = 50, d = i);
    }
```

Vu qu'il ne s'agit que d'un test, la pièce n'est pas paramétrisée au maximum et je ne trouve pas nécessaire d'y ajouté une license. Voici le résultat final: 

<div style="text-align:center">
  <img src="../images/module_3/antenne_test.png" alt="" style="width:90%;">
</a>  
</div>

Le fichier *.stl* est disponible [ici](images/module_3/test_antenna.stl).

Le but est d'imprimer plusieurs antennes inlinées de 30° chancunes (cet angle a été choisi arbitrairement). Chaque antenne a un diamètre différent. La plus fine a un diamètre de 1 milimètre, la plus épaisse de 5 milimètres. Les antennes entre les deux extrémitée évolue avec un pas de 0.5 mm. J'ai sauvegarder le fichier au format *.stl* en selectionnant le deuxième bouton en partant de la droite dans le menu au dessus du code. 

## 3.4 Slicer 


Comme on peut le lire [ici](https://nes-3d.fr/quest-ce-quun-logiciel-de-tranchage-3d/): "Globalement le Slicer permet la conversion du fichier que vous avez modélisé en une série de couches, plus ou moins fine, correspondant aux différentes étapes de construction de l’impression réelle.

Le logiciel ensuite génère un G-code qui contient toutes les instructions d’impression qui sera envoyé à l’imprimante.
C’est l’étape durant lequel les réglages liés à l’impression (température de buse et plateau, vitesse d’impression, ventilateur ..) sont effectués."

J'utilise [UltiMaker Cura](https://ultimaker.com/software/ultimaker-cura) comme slicer. Ce choix est simplement fait par habitude de ce logiciel. On peut simplement importer une pièce (fichier .stl) en faisant *Ctrl+O* et en selectionnant un fichier *.stl*.

Une fois la pièce importée, j'ai selectionné des paramètres d'impressions assez grossiers et rapide vu qu'il ne s'agit que d'un test. J'ai donc défini le profil sur 0.28 et le remplissage sur 10%. Evidement, je ne place pas de support car il serait difficile de les retirer sans casser les antennes et cela augmentera concidérablement le temps d'impressions. Il ne me suffit plus que d'appuyer sur le bouton *slice* en bas a droite.

<div style="text-align:center">
  <img src="../images/module_3/antenne_cura.png" alt="" style="width:90%;">
</a>  
</div>

La prévisualisation de l'impression est alors visible (cf. photo précédente). Il est possilble de voir couche par couche le chemin de l'imprimante en utilisant le curseur qui est a droite de l'écran.
Il ne me reste plus qu'a sauvegarder le fichier *.gcode* sur une carte sd. Le fichier *.gcode* est un simple fichier qui reprends les instructions pour l'imprimante sous forme de texte. 
A titre d'exemple, voici le début du *.gcode* généré pour cette impression. 

<div style="text-align:center">
  <img src="../images/module_3/gcode.png" alt="" style="width:30%;">
</a>  
</div>

## 3.5 L'impression

Le fichier *.gcode* étant générer, je peux lancer l'impression. Par expérience avec mon imprimante, je lance l'impression avec une température de plateau à **65°C** et une température de buse à **210°C** pour une impression avec du **PLA**.

Voici le résultat: 

<div style="text-align:center">
  <img src="../images/module_3/first_print.jpg" alt="" style="width:50%;">
</a>  
</div>


L'on peut clairement voir que l'antenne de 1mm ne s'est pas imprimer jusqu'au bout et que l'antenne de 1,5mm est de très mauvaise qualitée. 

En terme de flexion, l'antenne de 1,5mm et de 2mm se plient suffisament. 

<div style="text-align:center;">
<video controls muted>
<source src="../images/module_3/flexion_antenne.mp4" type="video/mp4">
</video>
</div>

![](images/module_3/flexion_antenne.mp4)


Je vais donc selectionner une antenne de 2mm de diamètre (la troisième en partant de la droite). Ce diamètre est le bon compromis entre qualité d'impression et flexion suffisante. 

## 3.6 Le resort de Cédric

Au FabLab, nous avons essayé d'imprimer le ressort que Cédric a dessiné pour la suspension de la voiture. La première tentative était un échec (l'angle de portance était trop important).

<div style="text-align:center">
  <img src="../images/module_3/ressort_fail.jpg" alt="" style="width:45%;">
</a>  
</div>

En corrigeant l'angle de portance, l'impression est de meilleure qualité et le ressort est bien flexible! 

<div style="text-align:center;">
<video width="480" height="270" controls muted>
<source src="../images/module_3/video.mp4" type="video/mp4">
</video>
</div>

![](images/module_3/video.mp4)

## 3.7 Montage
Ayant reçu de Cédric et Dimitri les autres pièces de la voiture, j'ai pu les importer dans le slicer pour l'impression. 

<div style="text-align:center">
  <img src="../images/module_3/assemblage_sliver.png" alt="" style="width:90%;">
</a>  
</div>


L'impression a duré 5 heures. J'ai lancé l'impression avec les mêmes paramètres que précédemment: une température de buse de 210°C et une température de plateau à 65°C. Le matériau utilisé était le PLA.
Voici le résultat: 

<div style="text-align:center">
  <img src="../images/module_3/tout_imprim.jpg" alt="" style="width:90%;">
</a>  
</div>

NB: J'ai placé du papier collant sur le plateau pour améliorer l'adhérence à celui-ci. 

L'impression est de bonne qualité. 
Et voilà enfin la voiture montée:

<div style="text-align:center">
  <img src="../images/module_3/assemblage1.jpg" alt="" style="width:60%;">
</a>  
</div>

<div style="text-align:center">
  <img src="../images/module_3/assemblage2.jpg" alt="" style="width:50%;">
</a>  
</div>

Je trouvais que les ressorts ne se plaçaient pas bien. J'ai donc décidé de refaire leurs designs sur Fusion360 avec des anneaux d'extremité, comme ceci:

<div style="text-align:center">
  <img src="../images/module_3/ressort_V2.jpg" alt="" style="width:90%;">
</a>  
</div>

Le fichier *.stl* est disponibe [ici](images/module_3/ressortv1.stl).

Il existe une fonction toute faite pour créer des ressorts sur Fusion360 dans l'onglet *créer*.
Voici le résultat:

<div style="text-align:center;">
<video width="480" height="270" controls muted>
<source src="../images/module_3/ressort_V2_flexion.mp4" type="video/mp4">
</video>
</div>

![](images/module_3/ressort_V2_flexion.mp4)

Une fois la voiture assemblée avec les nouveaux ressorts, nous pouvons tester si les suspensions fonctionnent bien: 

<div style="text-align:center;">
<video width="480" height="270" controls muted>
<source src="../images/module_3/suspension_mouvement.mp4" type="video/mp4">
</video>
</div>

![](images/module_3/suspension_mouvement.mp4)

## 3.8 Amélioration possible 

Une amélioration possible pourrait être le système de fixation des roues pour garantir une meilleure stabilité et solidité.


