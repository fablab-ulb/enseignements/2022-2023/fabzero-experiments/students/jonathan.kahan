# 4. Découpe assistée par ordinateur

Dans cette unité, nous avons appris à utiliser les découpeuses laser du Fablab de l'ULB.

## 4.1 Sécurité

Afin d'assurer la sécurité des personnes et prévenir tout risque d'incendie, il faut de prendre diverses précautions lors de l'utilisation des machines. Parmi celles-ci, il est important d'activer l'air comprimé et d'allumer l'extracteur de fumée, de rester à proximité de la machine jusqu'à la fin du processus de découpe, de connaître l'emplacement du bouton d'arrêt d'urgence ainsi que celui de l'extincteur. Il faut  également faire attention aux matériaux utilisés, certains pouvant émettre des fumées toxiques ou corrosives.
Toutes les consignes peuvent être retrouvées plus en détail [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md).

## 4.2 Pendant le cours - Group assignment

Lors du cours, j'ai utilisé la machine **Muse** avec mon groupe.

Les caractéristiques de la machine comprennent une **surface de découpe de 50x30cm, une hauteur maximale de 6cm et une puissance de 40W.** Un avantage notable est la présence d'un pointeur laser rouge pour faciliter le positionnement de l'esquisse par rapport au matériau à découper.

Pour la faire fonctionner, il est nécessaire de se connecter soit par câble Ethernet ou par un réseau Wi-Fi dédié et d'entrer dans son navigateur de recherche l'adresse IP renseignée sur l'écran de la machine pour accéder au programme. Les fichiers compatibles avec ce programme incluent SVG (vectoriel) ainsi que BMP, JPEG, PNG et TIFF (matriciels).

Il faut noter que si l'on utilise des fichiers SVG, l'esquisse sera importée à la fois en vecteur et en matrice, donc il faudra supprimer l'un des deux. Il est également important de prêter attention au "laser kerf", c'est-à-dire la différence entre les mesures sur l'esquisse et la découpe réelle en raison de la largeur du laser.

Lors de la découpe laser, ***deux paramètres importants à prendre en compte sont la puissance et la vitesse.** La puissance se réfère à la quantité d'énergie délivrée par le laser, tandis que la vitesse fait référence à la vitesse à laquelle le laser se déplace sur le matériau. Ces deux paramètres sont interdépendants et **leur combinaison détermine la qualité et la précision de la découpe.** Si la puissance est trop faible par rapport à la vitesse, le laser risque de ne pas couper complètement le matériau. En revanche, une puissance excessive peut brûler ou endommager le matériau. De même, une vitesse excessive peut entraîner une découpe inexacte ou une surface rugueuse, tandis qu'une vitesse trop faible peut causer une fusion excessive ou des marques indésirables sur le matériau. **Il est donc important de trouver le bon équilibre entre la puissance et la vitesse pour garantir une découpe précise et de qualité.**

Pour tester l'influence de la puissance et de la vitesse de découpe sur un matériau donné, il est courant d'imprimer une plaquette de test. Cette plaquette est conçue pour permettre de varier les paramètres de découpe et d'obtenir un échantillon de chaque combinaison de paramètres. Une fois la plaquette de test imprimée, on peut observer la qualité de la découpe pour chaque combinaison de puissance et de vitesse et déterminer les paramètres optimaux pour le matériau en question. Cette méthode permet d'optimiser rapidement les paramètres de découpe pour des projets futurs et d'éviter des erreurs coûteuses en matériau et en temps. De plus, la plaquette de test peut être conservée pour une référence future, permettant ainsi de reproduire facilement les résultats de la découpe optimale pour ce matériau spécifique.

Le test a été téléchargé sur ce [site](https://archive.fablabo.net/wiki/Exp%C3%A9rimentation_du_mode_raster).

Voici le résultat:

<div style="text-align:center">
  <img src="../images/module_4/resTestD%C3%A9coupeuselosquality.jpg" alt="" style="width:50%;">
</a>  
</div>

Ce test permet de voir le résultat pour une vitesse et une pouissance donnée.
Pour ce matériaux (carton), une vitesse de 75% semble être le minimum tolérable pour avoir une bonne qualité d'impression. Une puissance de 25% semble suffisante également.

## 4.3 Le dessin vectoriel et matriciel

Le dessin matriciel et vectoriel sont deux types de formats d'image différents. Le dessin matriciel, également appelé bitmap, est constitué de pixels individuels, chacun ayant sa propre couleur et position. Les images matricielles sont créées en utilisant des logiciels tels que Photoshop ou GIMP et sont souvent utilisées pour des images complexes, des photographies ou des illustrations.

Le dessin vectoriel, quant à lui, est créé à partir de formes géométriques comme des lignes, des courbes et des points. Les images vectorielles sont créées à l'aide de logiciels de dessin vectoriel tels que Adobe Illustrator ou Inkscape. Les images vectorielles peuvent être redimensionnées sans perte de qualité, car elles sont basées sur des formes géométriques plutôt que sur des pixels.

## 4.5 Après le cours - Individual assignment

Le but ici est de desginer et découper un [kirigami](https://en.wikipedia.org/wiki/Kirigami#:~:text=Kirigami%20(%E5%88%87%E3%82%8A%E7%B4%99)%20is%20a,typically%20does%20not%20use%20glue.). 

J'ai décidé de créer un dé à jouer en utilisant la technique du kirigami. Cette technique permet de découper et de plier du papier pour créer des formes en 3D.

J'ai utilisé openSCAD pour créer le modèle 2D du dé. J'ai pris le temps de bien paramétriser mon document afin de pourvoir le modifier facilement en cas de problème plus tard.

Voici comment créer une des six face:

<div style="text-align:center">
  <img src="../images/module_4/Premi%C3%A8reFaceD%C3%A9.png" alt="" style="width:80%;">
</a>  
</div>

Pour réaliser les autres faces, je me suis basé sur la première. Il suffit de lui faire subir une translation et de changer le nombre de trous. Voici ce que donne le résultat final: 

<div style="text-align:center">
  <img src="../images/module_4/d%C3%A9_total.png" alt="" style="width:80%;">
</a>  
</div>

Le fichier sous licence est téléchargable [ici](images/module_4/de_total.scad).

Attention, comme respecté dans cet exemple, la somme des côtés opposés d'un dé est toujours égale à sept. 

Une fois que j'ai terminé la modélisation du dé, j'ai exporté le fichier en format *.dxf*. Ce format de fichier est largement utilisé pour les dessins en 2D et peut être facilement importé dans de nombreux logiciels de dessin vectoriel.

Ensuite, j'ai importé le fichier *.dxf* dans Inkscape. Inkscape est un logiciel de dessin vectoriel libre et gratuit qui est idéal pour la création de modèles de kirigami.

<div style="text-align:center">
  <img src="../images/module_4/de_dans_inscape.png" alt="" style="width:80%;">
</a>  
</div>

Dans Inkscape, j'ai défini des couleurs pour les arêtes du modèle de dé. Cela me permetra de donner des instructions spécifiques sur les parties à découper et à plier (c'est à dire, une puissance et une vitesse différente en fonction de la vitesse).

<div style="text-align:center">
  <img src="../images/module_4/de_dans_inkscape_with_color.png" alt="" style="width:80%;">
</a>  
</div>

J'ai sauvegardé ce fichier en format *.svg*.

Il ne me reste plus qu'à lancer la découpe ! Je vérifie que toutes les consignes de sécurité sont respectées et je me connecte au Wi-Fi de l'imprimante comme indiqué dans la documentation. Ensuite, je me connecte à la découpeuse dans mon navigateur en saisissant l'adresse IP de celle-ci (on peut la trouver sur l'écran de contrôle). Voici à quoi ressemble le logiciel en ligne :

<div style="text-align:center">
  <img src="../images/module_4/logiciel_muse.png" alt="" style="width:80%;">
</a>  
</div>

Dans *File*, j'importe mon modèle .svg de dé. Le programme reconnaît automatiquement les couleurs présentes et je peux choisir les paramètres de découpe (vitesse et puissance) pour chacune d'entre elles.

<div style="text-align:center">
  <img src="../images/module_4/parameters_used_to_cut.png" alt="" style="width:80%;">
</a>  
</div>

Pour découper en (**rouge**), j'ai utilisé une grande puissance et une vitesse un peu plus faible. Pour tracer les traits de pliage (**vert**), j'ai utilisé une très grande vitesse et une puissance très faible.

Actuellement, j'ai fixé le dé en utilisant du papier collant plié en angle droit à l'intérieur des arêtes. Une manière d'améliorer cela serait d'ajouter de petites bandes sur les bords pour faciliter la construction du dé.

Finalement, j'ai obtenu mon dé à jouer en kirigami. C'est un 6/6 pour cette documentation !

<div style="text-align:center;">
<video width="480" height="270" controls muted>
<source src="../images/module_4/lancement_de_6sur6.mp4" type="video/mp4">
</video>
</div>


<div style="text-align:center">
  <img src="../images/module_4/de1.jpg" alt="" style="width:55%;">
</a>  
</div>
