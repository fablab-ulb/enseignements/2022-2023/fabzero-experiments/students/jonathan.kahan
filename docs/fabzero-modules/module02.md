# 2. Conception Assistée par Ordinateur (CAO)

## 2.1 Notre projet

En collaboration avec [Cédric Luppens](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/cedric.luppens/) et [Dimitri Debauque](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/dimitri.debauque/), nous avons décidé de modéliser une voiture. Celle-ci sera composée de 4 suspensions (spirales) flexibles ainsi que d'une antenne flexible. Je suis en charge de dessiner le chassi de la voiture, Cédric s'occupe des suspensions et finalement, Dimitri est en charge des roues et essieux.

## 2.2 Logiciel CAO disponible 

Due à mes projets précédents réalisés à l'université, j'ai déjà acquéri une bonne connaissance de SolidWorks et Fusion360. Cependant, ces deux logiciels sont privés et je n'y aurai par conséquent plus accès l'année prochaine lorsque je ne serai plus étudiant. Ce module était donc l'occasion pour moi d'apprendre de nouveaux logiciels **open-source**. La [page de cours Fabzero pour ce module](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/fabzero-modules/computer-aided-design/) nous propose deux logiciels différents:

- OpenSCAD 
- FreeCAD

OpenSCAD est moins visuel et demande plus d'effort de calcul à l'utilisateur, tandis que FreeCAD ressemble beaucoup plus aux logiciels que j'ai déjà utilisé précédement. OpenSCAD me semblais plus difficile à prendre en mains mais également très puissant une fois maitrisé. C'est pourquoi j'ai décidé d'utiliser **OpenSCAD**. 

## 2.3 OpenSCAD 

OpenSCAD peut être téléchargé très facilement [ici](https://openscad.org/) en quelques secondes et ne demande pas de commentaire particulier. 

Une fois le logiciel ouvert, voici ce qu'on peut voir. 

<div style="text-align:center">
  <img src="../images/module_2/%C3%A9diteur.png" alt="" style="width:90%;">
</a>  
</div>


Un éditeur de code est disponible sur la gauche tandis qu'un prévisualisation de notre modèle 3D est disponible sur la droite. 

## 2.4 Réalisation du chassi de la voiture

Le code complet peut-être téléchargé [ici](images/module_2/module2_voiture.scad).

Afin de réaliser le chassi de la voiture, je vais commencer par créer un parallélépipède rectangle.  Paradoxalement, un parallélépipède est créé en appelant la fonction `cube()`, comme on peut le voir sur le code et l'image ci-dessous. Toutes les fonctions peuvent être retrouvées dans la [cheat sheet](https://openscad.org/cheatsheet/).

```
//cube parameters
cube_lar = 50;
cube_long = 50;
cube_h = 10; 
cube(size=[cube_lar, cube_long, cube_h], center = false); 
```

<div style="text-align:center">
  <img src="../images/module_2/cube.png" alt="" style="width:90%;">
</a>  
</div>

NB: J'ai défini toutes les dimensions dans des variables afin de pouvoir très facilement adapter mes dimensions aux pièces de Cédric et Dimitri si besoin. De plus, cela permettra une adaptation facile aux imprécisions de l'imprimante 3D. Par défaut, les unités sont en milimètres. 

J'ai ensuite voulu arrondir l'avant de la voiture. Pour ce faire, j'ai créé un `union(){}` entre le cube déjà créé et un cylindre.

<div style="text-align:center">
  <img src="../images/module_2/cube_and_front.png" alt="" style="width:90%;">
</a>  
</div>

On peut voir que le cylindre n'est pas aligné avec la base du chassi mais centré à l'origine du repère. J'ai donc utilisé la fonction `translate([x,y,z])` pour le déplacer. Comme on peut le voir sur l'image ci-dessous, j'ai utilisé les variables qui définissent les dimensions du cube de base pour donner les coordonnés et le diamètre du cylindre (*ligne 12 et 13*). **Par conséquent, si les dimensions du cube de base sont changées, la taille et la position du cylindre s'adapteront automatiquement.**

NB: La fonction $fn est utilisée pour définir le nombre de segments utilisés pour approximer les formes géométriques courbes ou circulaires. Elle prend un nombre entier en argument, qui représente le nombre de segments utilisé pour représenter une courbe ou un cercle.

<div style="text-align:center">
  <img src="../images/module_2/cube_and_front_aligned_not_pot_echap.png" alt="" style="width:90%;">
</a>  
</div>

**Pro tips**: Comme on peut le voir dans l'image précédente (*ligne 16*), je voulais rajouter deux pots d'échappements à l'arrière de la voiture en utilisant la fonction `rotate()` sur un cylindre. Cependant, malgré la ligne écrite, je ne le voyais pas dans la visualisation 3D. Il est alors possible de rendre une partie de notre pièce transparente en utilisant `%`. J'ai donc rendu le cube principal transparent comme ceci (*`%` à la ligne 11*): 

<div style="text-align:center">
  <img src="../images/module_2/pourcent.png" alt="" style="width:90%;">
</a>  
</div>


J'ai pu alors voir où se trouvait le pot d'échappement et ajuster mon code pour le placer au bon endroit. Une fois de plus, ses coordonnées se basent sur les dimensions du cube de base pour être automatiquement modifié si les dimensions du cube sont changées. Voici la pièce obtenue une fois les deux pots rajoutés.

<div style="text-align:center">
  <img src="../images/module_2/chassis_and_echap_pot.png" alt="" style="width:90%;">
</a>  
</div>

Il est maintenant temps de percer les trous dans le chassi pour acceuillir les roues et suspensions dessinées par Cédric et Dimitri. Pour cela, j'utilise la fonciton `difference(){}`. Voici le code et le résultat: 

<div style="text-align:center">
  <img src="../images/module_2/wrong_hole.png" alt="" style="width:90%;">
</a>  
</div>

On peut voir que le trou n'est pas complet comme attendu. Le cylindre représentant l'avant de la voiture vient le reboucher. Cela est due au fait que l'opération *différence* n'est pas commutative ! Il faut donc d'abord assembler toutes les pièces avant de retirer de la "matière". J'ai donc modifié l'ordre de mon code comme ceci. Le résultat est bien celui qui est attendu. 

<div style="text-align:center">
  <img src="../images/module_2/final_without_txt.png" alt="" style="width:90%;">
</a>  
</div>


Finalement, j'ai rajouté une antenne flexible. A nouveau, ses dimensions et sa position sont totallement paramétrées. 

<div style="text-align:center">
  <img src="../images/module_2/antenna.png" alt="" style="width:40%;">
</a>  
</div>

**Pro tips**: Il arrive que les trous ne rende pas bien lors de la visualisation 3D (cf. image ci-dessous). Pour eviter cela, utiliser la touche `F6` au lieu de `F5` pour compiler votre code. Cela prendra un peu plus de temps mais permettra d'éviter ce problème. 

<div style="text-align:center">
  <img src="../images/module_2/f5.png" alt="" style="width:40%;">
</a>  
</div>

## 2.5 License 

Pour finir, j'écris la license [CC BY-SA] de mon code en début de code comme ceci: 
```
// File : module2_voiture.scad

//Author : Jonathan Kahan

// Date : 24 Février 2023

// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
```

A propos de la licence CC BY-SA que j'ai rajouté: *This license allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, so long as attribution is given to the creator. The license allows for commercial use. If you remix, adapt, or build upon the material, you must license the modified material under identical terms.
CC BY-SA includes the following elements:
BY  – Credit must be given to the creator. SA  – Adaptations must be shared under the same terms*. 

Tous les types de licence peuvent être retrouvés [ici](https://creativecommons.org/about/cclicenses/).