// File : module2_voiture.scad

//Author : Jonathan Kahan

// Date : 24 Février 2023

// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

$fn = 150;

size = 40;
d1 = 5;
e = 0.05;

union(){
//Face number 1
    union(){
        difference(){
            square(size, center = true);
            square(size-e, center = true);
        }
        
        difference(){
            circle(d = d1);
            circle(d = d1-e);
        }
    }
//Face number 3
    translate([size,0,0])
        union(){
            difference(){
                square(size, center = true);
                square(size-e, center = true);
            }
            
            difference(){
                circle(d = d1);
                circle(d = d1-e);
            }
            
            translate([0,-size/4,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
                
            translate([0,+size/4,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
        }
        
 //Face number 6
    translate([2*size,0,0])
    union(){
        difference(){
            square(size, center = true);
            square(size-e, center = true);
        }
        translate([-size/5,0,0]){
            difference(){
                circle(d = d1);
                circle(d = d1-e);
            }
        
            translate([0,-size/4,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
                
            translate([0,+size/4,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
        }
        
        translate([size/5,0,0]){
            difference(){
                circle(d = d1);
                circle(d = d1-e);
            }
        
            translate([0,-size/4,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
                
            translate([0,+size/4,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
        }        
    }
    
//Face number 2
    translate([0,size,0])
        union(){
            difference(){
                square(size, center = true);
                square(size-e, center = true);
            }
            translate([0,size/7,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
            
            translate([0,-size/7,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
        }
    
 //Face number 5
    translate([0,-size,0])
    union(){
        difference(){
            square(size, center = true);
            square(size-e, center = true);
        }
        translate([-size/5,0,0]){        
            translate([0,-size/4,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
                
            translate([0,+size/4,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
        }
        
        translate([size/5,0,0]){       
            translate([0,-size/4,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
                
            translate([0,+size/4,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
        }
  
        difference(){
            circle(d = d1);
            circle(d = d1-e);
        }
    }
        
  //Face number 5
    translate([-size,0,0])
    union(){
        difference(){
            square(size, center = true);
            square(size-e, center = true);
        }
        translate([-size/5,0,0]){        
            translate([0,-size/4,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
                
            translate([0,+size/4,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
        }
        
        translate([size/5,0,0]){       
            translate([0,-size/4,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
                
            translate([0,+size/4,0])
                difference(){
                    circle(d = d1);
                    circle(d = d1-e);
                }
            }
    }
        
}




























