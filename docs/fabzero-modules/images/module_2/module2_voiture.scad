// File : module2_voiture.scad

//Author : Jonathan Kahan

// Date : 24 Février 2023

// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

$fn = 200;

//cube parameters
cube_lar = 50; //Largeur du cube
cube_long = 50; //Longueur du cube
cube_h = 10; //Hauteur du cube

pot_echap_diam = 5; //Diamètre du pot d'échappement
pot_echap_long = 5; //Longueur du pot d'échapement 

suspension_hole = 8; //Diamètre du trou pour la suspension

antenna_h = 30; //Longeuer de l'antenne
antenna_d = 2; //Diamètre de l'antenne

difference(){
    union(){
        cube(size=[cube_lar, cube_long, cube_h], center = false);
            translate([cube_lar/2, 0,0])
        cylinder(cube_h,d=cube_lar, $fn = 200);

        translate([cube_lar/4,cube_long+pot_echap_long,cube_h/2])
            rotate([90,0,0])
                cylinder(cube_h,d=pot_echap_diam);

        translate([cube_lar/4*3,cube_long+pot_echap_long,cube_h/2])
            rotate([90,0,0])
                cylinder(cube_h,d=pot_echap_diam);

        translate([cube_lar/4*3,cube_long+pot_echap_long,cube_h/2])
            rotate([90,0,0])
                cylinder(cube_h,d=pot_echap_diam);
        
        translate([cube_lar/2,7/10*cube_long, cube_h-5])
            rotate([-30,0,0])
                cylinder(antenna_h, d=antenna_d);

    }
    translate([cube_lar/4,cube_long/4*3,0])
        cylinder(cube_h,d=suspension_hole);
    
    translate([cube_lar/4,0,0])
        cylinder(cube_h,d=suspension_hole);
    
    translate([cube_lar/4*3,0,0])
        cylinder(cube_h,d=suspension_hole);
    
    translate([cube_lar/4*3,cube_long/4*3,0])
        cylinder(cube_h,d=suspension_hole);
}

