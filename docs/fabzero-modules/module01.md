# 1. Gestion de projet et documentation
## 1.1 Interface de ligne de commande CLI
Le but de cette première unité est de se familiariser avec quelques commandes UNIX fondamentales, en utilisant le terminal.  

La première étape pour moi a été de télécharger BASH (Unix Shell). Cela permet de lancer des outils Linux et de développer ses propres scripts Shell directement sous le système d’exploitation Windows. 

Son téléchargement n'a pas été compliqué grâce à ce [tuto]( https://korben.info/installer-shell-bash-linux-windows-10.html). Les principales étapes étaient les suivantes:

* Vérifier que la version de Windows installée peut supporter Bash. Cela est bien le cas sur mon PC. 
* Activer le mode développeur 
* Dans l'onglet *Activer ou désactiver des fonctionnalités Windows* des paramètres Windows, activer "*Sous-système Windows pour Linux (bêta)*"
* Exécuter la commande **bash** dans la barre de recherche
* Accepter la licence de Canonical en entrant O dans le terminal 
* **Bash sur Ubuntu sur Windows** est maintenant installé et peut être utilisé 

Une fois l'interpréteur de commande installé, j'ai pu m'entrainer en rentrant quelques commandes, comme on peut le voir sur le *screenshot* ci-dessous. Une command line est une combinaison de plusieurs élements : command, option et argument.


<div style="text-align:center">
  <img src="../images/module_1/command_training_module1.png" alt="" style="width:40%;">
</a>  
</div>

Voici les étapes que j'ai utilisées et à quoi elles ont servi: 

* La commande **pwd** pour voir dans quel dossier je me trouvais (on peut voir que j'étais dans le dossier */root*)
* J'ai ensuite utilisé la commande **cd** pour me placer dans le dossier */home*
* J'ai ensuite créé deux dossiers (*test_directory_1* et *test_directory_2*  dans le dossier home en utilisant la commande **mkdir**
* Pour vérifier que les dossiers s'étaient bien créés, j'ai utilisé la commande **ls** avec comme argument */home*. Les dossiers et fichiers présents dans ce répertoire se sont alors affichés. Les deux dossiers créés étaient bien présents. 
* J'ai ensuite utilisé la commande **cp** avec comme argument *example_file.txt test_directory_1* pour copier le fichier *example_file.txt* dans le *dossier test_directory_1*
* En utilisant une fois de plus la commande **ls**, on peut voir que le fichier .txt a bien été copié. 
* La commande **mv** a été utilisée pour déplacer un document dans un dossier et la commande **rm** pour le supprimer. 

Cet exercice m'a permis de découvrir les commandes de bases dans un terminal et de me familliariser avec celles-ci. Evidemment, il existe beaucoup plus de commandes que celles utilisées ici, voici la [documentation](https://www.tjhsst.edu/~dhyatt/superap/unixcmd.html). 

## 1.2 Documentation et contrôle des versions
Le but de cette seconde unité est d'apprendre quelques outils simples mais puissants pour écrire une meilleure documentation et la partager en utilisant un système de contrôle de version.

Comme expliqué sur [la page FabZero](https://gitlab.com/fablab-ulb/enseignements/fabzero/basics/-/blob/main/documentation.md) du cours, la documentation est très importante car elle permet de: 

* Documenter le processus d'apprentissage (échecs et réussites)
* Mettre en évidence le processus utilisé plutôt que le résultat final
* Documenter pour notre futur moi - la mémoire du poisson rouge
* Partager pour aider les autres

### 1.2.1 Rédiger une documentation en Markdown
J'ai commencé par installer un éditeur de texte open-source  qui permet d'obtenir un aperçu intégré du format Markdown. J'ai choisi **VScodium** arbitrairement. Pour m'entrainer à utiliser le *Command Line Interface*, j'ai téléchargé **VScodium** en utilisant une ligne de commande.

<div style="text-align:center">
  <img src="../images/module_1/VSCodium_install.png" alt="" style="width:40%;">
</a>  
</div>

Comme l'on peut le voir dans l'image, la ligne de commande à utiliser est "*Winget install vscodium*" puis "*y*" pour accepter les "*termes du contrat source*"

Je me suis ensuite entrainé à écrire quelques lignes dans différentes polices, à faire des titres et sous-titres, à faire des bullet-points,...
J'ai également suivi ce [tuto](https://www.markdowntutorial.com/) qui permet une bonne prise en main du langage **Markdown**.

### 1.2.2 Images GraphicsMagick

Des images sont souvent utilisées dans les documentations. Les smartphones actuels faisant des images entre 5MB et 10MB, il est important de savoir comment modifier nos images pour réduire leur taille et par conséquent, leur impact. En effet, une résolution bien inférieure (quelques centaines de Ko) est souvent plus que suffisante. Il est possible de redimensionner une image (et pas que) sur le CLI en utilisant GraphicsMagick. Malheureusement, son téléchargement est bloqué sur mon ordinateur par Windows (-1 pour la team Windows!). 

<div style="text-align:center">
  <img src="../images/module_1/GraphicsMagicks_blocked.png" alt="" style="width:40%;">
</a>  
</div>

Pour contourner ce problème, j'ai utilisé la machine virtuelle Linux installée précédement. Le téléchargement a été fait en quelques minutes grâce a ce [tuto](https://www.howtoinstall.me/ubuntu/18-04/graphicsmagick/) 

(+1 pour la team Linux!). Voici en résumé les lignes de code à utiliser:
```
sudo apt update
sudo apt install graphicsmagick 
```
Une fois le logiciel téléchargé, je me suis entrainé à l'utiliser. Pour ce faire, j'ai réduit la taille d'une image.

<div style="text-align:center">
  <img src="../images/module_1/reduce_image_size.png" alt="" style="width:40%;">
</a>  
</div>

Comme on peut le voir sur l'image précédente, j'ai suivi les étapes suivantes:

* Je me suis placé dans le dossier que j'ai créé pour ce cours en utilisant la commande **cd**
* J'ai ensuite affiché tous les fichiers en utilisant la commande **ls**
* J'ai choisi de modifier le fichier *man.png*. J'ai donc entré la commande *gm convert -resize 600x600 man.png man_small.jpg.*
* J'ai à nouveau utilisé la commande **ls** pour afficher les fichiers du dossier. On peut voir que le fichier man_small_jpg est apparu. 
Cette action à permis de réduire la taille de l'image de 77Ko à 18Ko. L'image stockée prend donc plus ou moins 4 fois moins de place! Cela sera particulièrement utile pour les images de plusieurs MB. 

D'autres commandes existent, il est possible de les trouver en utilisant la commande `gm -help`. 

Le seul désavantage de cette méthode est que je suis obligé de travailler dans la machine virtuelle Linux et que je ne peux donc pas accéder aux images stockées en dehors de celle-ci, par exemple, dans mon répertoire *Pictures*. 

### 1.2.3 Déploiement automatique et contrôle de version - GIT & GitLab

Lors de ce cours, nous allons utiliser l'interface graphique (GUI) Gitlab.com pour mettre à jour notre site web. Les étapes sont les suivantes : se connecter à GitLab, accéder à sa page personnelle, modifier les fichiers markdown (.md).

De plus, j'ai activé les notifications par e-mail de GitLab, comme recommandé. Je peux donc garder une trace des modifications apportées au site web.

Un protocol SSH est utilisé pour créer une connexion sécurisée entre le serveur GitLab et mon ordinateur. 
Ce protocol SSH va nous permettre de créer une paire de clés, l’une étant privée et l’autre publique. Pour créer ces clés, il faut utiliser un certain cipher, celui qui est conseillé est le “ED25519”. Lors de la création des clés, nous devons préciser un passphrase, il s’agit en réalité de notre mot de passe. Grâce à ce mot de passe et à leur cipher ils vont créer la paire de clés ([source](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module01/#fn:4)).

J'ai créé les clés en ouvrant un terminal GitBash dans le dossier où je voulais cloner mon projet pour y travailler. J'ai ensuite suivi ce [tuto](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-via-ssh). Voici la ligne de commande utilisée: 

<div style="text-align:center">
  <img src="../images/module_1/cle_generation.png" alt="" style="width:40%;">
</a>  
</div>

Il est maintenant nécessaire d'ajouter la clé publique SSH à mon compte GitLab tout en veillant à conserver la clé privée. Cela peut être fait dans les préférences de GitLab, il y a un onglet SSH keys où on peut ajouter la clé. 

Une fois la connexion sécurisée, j'ai pu cloner le projet  "localement" sur mon ordinateur. Il suffit de se rendre sur mon profil GitLab et de copier le lien *Clone with SSH* sous l'onglet *Clone*. En ouvrant un terminal **Git Bash** dans le dossier où l'on veut la copie du projet, il suffit de rentrer la commande `git clone le_lien_copié`, comme montré sur l'image ci-dessous: 

<div style="text-align:center">
  <img src="../images/module_1/git_clone.png" alt="" style="width:40%;">
</a>  
</div>


Il est maintenant possible et facile de travailler sur la copie du projet qui se trouve sur mon ordinater. Certaines commandes sont à connaitre: 

* `git pull` permet de télécharger la dernière version du projet 
* `git add -A` permet d'ajouter tous les changements qui ont été faits
* `git commit -m "message"` permet de commenter l'upload 
* `git push` envoie nos changements au serveur 

## 1.3 Principes de gestion de projet

Voici trois points que j'ai retenus de cette unité et que j'essayerai d'appliquer: 

* Créer la documentation au fur et à mesure que l'on travaille, ne pas attendre la fin du module pour créer toute la documentation
* Penser à concevoir et planifier mes projets en utilisant des tâches plus petites et modulaires qui seront organisés selon leur ordre d'importance
* "Supply-Side Time Management: SSTM is a time organization method to divides weekly available work time into discrete segments, then fills those segments with discrete tasks to be completed for the week.”